package wireless.network.wifi.discovery;

import java.io.Serializable;

public class DiscoveryRequest<T> implements Serializable {

    private String name;
    private T payload;

    public DiscoveryRequest(String name, T payload) {
        this.name = name;
        this.payload = payload;
    }

    public String getName() {
        return name;
    }

    public T getPayload() {
        return payload;
    }
}
