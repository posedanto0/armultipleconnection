package wireless.network.wifi.discovery;

import java.io.Serializable;

public class DiscoveryReply implements Serializable {

    private String ip;
    private int port;

    public DiscoveryReply(String ip, int port) {
        this.ip = ip;
        this.port = port;
    }

    public String getIp() {
        return ip;
    }

    public int getPort() {
        return port;
    }
}
