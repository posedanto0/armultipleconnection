package wireless.network.wifi;

import wireless.network.wifi.utils.Payload;

public interface Communicator {

    void sendMessage(long recipientId, Payload<?> payload);
    void sendBroadcastMessage(Payload<?> payload);
    void sendUiEvent(Payload<?> payload);
}
