package wireless.network.wifi.utils;

import java.io.Serializable;

public class Payload<T> implements Serializable {

    private T payload;

    public Payload(T payload) {
        this.payload = payload;
    }

    public T getPayload() {
        return payload;
    }


}
