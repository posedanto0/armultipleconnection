package wireless.network.wifi.utils;

import java.io.Serializable;

public class IncomingServerMessage implements Serializable {

    private String type;
    private Object message;

    public IncomingServerMessage(String type, Object message) {
        this.type = type;
        this.message = message;
    }

    public String getType() {
        return type;
    }

    public Object getMessage() {
        return message;
    }
}
