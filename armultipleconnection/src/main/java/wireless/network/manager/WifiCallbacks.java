package wireless.network.manager;

import wireless.network.wifi.utils.Payload;

public interface WifiCallbacks {
    void onDiscoveryTimeout();
    void onServerDiscovered();
    void onSessionStart();
    void onMessageReceived(Payload<?> payload);
    void onClientConnected(Payload<?> payload);
}
