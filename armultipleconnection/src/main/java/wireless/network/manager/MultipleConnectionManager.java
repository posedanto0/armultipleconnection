package wireless.network.manager;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import de.greenrobot.event.EventBus;
import wireless.network.bluetooth.BluetoothManager;
import wireless.network.wifi.client.LocalClient;
import wireless.network.wifi.server.LocalServer;
import wireless.network.wifi.utils.Payload;

public class MultipleConnectionManager implements LocalServer.OnUiEventReceiver {
    private ConnectionState currentState = ConnectionState.DISABLE;
    private LocalClient client;
    private LocalServer server;
    private LocalServer localServer;
    private WifiCallbacks wifiCallbacks;
    private BluetoothCallbacks bluetoothCallbacks;
    private Context context;
    private BluetoothManager bluetoothManager;

    private static MultipleConnectionManager manager;

    public static MultipleConnectionManager getMultipleConnectionManager(Context context, FailureCallback failureCallback){
        //проверка доступности
        manager = new MultipleConnectionManager();
        manager.context = context;
        return manager;
    }

    public void selectWiFiMode(@NonNull WifiCallbacks wifiCallbacks) {
        this.wifiCallbacks = wifiCallbacks;
    }

    public void  selectBluetoothMode(@NonNull String UUID, @NonNull BluetoothCallbacks bluetoothCallbacks) {
        this.bluetoothCallbacks = bluetoothCallbacks;
        bluetoothManager = new BluetoothManager(context, bluetoothCallbacks);
        bluetoothManager.setUUIDappIdentifier(UUID);
        if(!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this);
        bluetoothManager.setNbrClientMax(5);
    }

    //WIFI SECTION

    public void startServerDiscovering() {
        if (currentState != ConnectionState.DISABLE) {
            disable();
        }

        currentState = ConnectionState.SERVER_DISCOVERING;
        localServer = new LocalServer(context);
        localServer.setReceiver(this);
        localServer.init();
    }

    public void startServerMessaging(Class session) {
        if (currentState != ConnectionState.SERVER_DISCOVERING) {
            //error
            return;
        }

        currentState = ConnectionState.SERVER_MESSAGING;
        //localServer.setSession(GameSession.class);
        localServer.setSession(session);
        server = new LocalServer(context);
        server.setReceiver(new LocalServer.OnUiEventReceiver() {
            @Override
            public void onUiEvent(Payload<?> payload) {
                //Toast.makeText(context, "EVENT: " + payload.getPayload(), Toast.LENGTH_LONG).show();
            }

            @Override
            public void onClientConnected(Payload<?> payload) {
                // not interested (not in this version)
            }
        });
    }

    private void startClientMessaging() {
        if (currentState != ConnectionState.CLIENT_DISCOVERING) {
            //error
            return;
        }

        currentState = ConnectionState.CLIENT_MESSAGING;
        client = new LocalClient(context);
        client.setReceiver(new LocalClient.MessageReceiver() {
            @Override
            public void onMessageReceived(Payload<?> payload) {
                if (context != null)
                    Toast.makeText(context, "" + payload.getPayload(), Toast.LENGTH_LONG).show();
                else
                    Log.e("USER", "Received but context is null: " + payload.getPayload());
            }
        });
    }

    public void startClientDiscovering(@NonNull Payload<?> discoveryMessage) {
        if (currentState != ConnectionState.DISABLE) {
            disable();
        }

        currentState = ConnectionState.CLIENT_DISCOVERING;
        LocalClient localClient = new LocalClient(context);
        localClient.connect(discoveryMessage);
        localClient.setDiscoveryReceiver(new LocalClient.DiscoveryStatusReceiver() {
            @Override
            public void onDiscoveryTimeout() {
                wifiCallbacks.onDiscoveryTimeout();
            }

            @Override
            public void onServerDiscovered() {
                wifiCallbacks.onServerDiscovered();
            }

            @Override
            public void onSessionStart() {
                startClientMessaging();
                wifiCallbacks.onSessionStart();
            }
        });
        localClient.setReceiver(new LocalClient.MessageReceiver() {
            @Override
            public void onMessageReceived(Payload<?> payload) {
                wifiCallbacks.onMessageReceived(payload);
            }
        });
    }

    public void disable() {
        if (currentState != ConnectionState.DISABLE) {
            if (client != null) {
                client.shutdown();
                client = null;
            }
            if (server != null) {
                server.shutdown();
                server = null;
            }
            if (localServer != null) {
                localServer.shutdown();
                localServer = null;
            }
        }

        if (EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().unregister(this);

        currentState = ConnectionState.DISABLE;
    }

    private void sendMessage(Payload<?> payload) {
        if (currentState == ConnectionState.SERVER_MESSAGING)
            server.sendLocalSessionEvent(payload);
        else if (currentState == ConnectionState.CLIENT_MESSAGING)
            client.sendSessionMessage(payload);
    }

    //server callback
    @Override
    public void onUiEvent(Payload<?> payload) {

    }

    //server callback
    @Override
    public void onClientConnected(Payload<?> payload) {
        wifiCallbacks.onClientConnected(payload);
    }

    //BLUETOOTH SECTION

    public boolean isBluetoothAvailable(){
        if(!bluetoothManager.checkBluetoothAviability()){
            if (bluetoothCallbacks != null)
                bluetoothCallbacks.onBluetoothNotSupported();
            return false;
        }
        return true;
    }

    public void setTimeDiscoverable(int timeInSec){
        bluetoothManager.setTimeDiscoverable(timeInSec);
    }

    public void startDiscovery(Activity activity){
        bluetoothManager.startDiscovery(activity);
    }

    public void scanAllBluetoothDevice(){
        bluetoothManager.scanAllBluetoothDevice();
    }

    public void disconnectClient(){
        bluetoothManager.disconnectClient(true);
    }

    public void disconnectServer(){
        bluetoothManager.disconnectServer(true);
    }

    public void createServeur(String address){
        bluetoothManager.createServeur(address);
    }

    public void selectServerMode(){
        bluetoothManager.selectServerMode();
    }
    public void selectClientMode(){
        bluetoothManager.selectClientMode();
    }

    public BluetoothManager.TypeBluetooth getTypeBluetooth(){
        return bluetoothManager.mType;
    }

    public BluetoothManager.TypeBluetooth getBluetoothMode(){
        return bluetoothManager.mType;
    }

    public void createClient(String addressMac){
        bluetoothManager.createClient(addressMac);
    }

    public void setMessageMode(BluetoothManager.MessageMode messageMode){
        bluetoothManager.setMessageMode(messageMode);
    }
}
