package wireless.network.manager;

public enum Errors {
    CallbacksNotFound, WiFiDisabled, BluetoothDisabled, BluetoothNotSupported
}
