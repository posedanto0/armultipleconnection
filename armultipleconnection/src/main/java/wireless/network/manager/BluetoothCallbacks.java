package wireless.network.manager;

import android.bluetooth.BluetoothDevice;

public interface BluetoothCallbacks {
    void onBluetoothDeviceFound(BluetoothDevice device);
    void onClientConnectionSuccess();
    void onClientConnectionFail();
    void onServerConnectionSuccess();
    void onServerConnectionFail();
    void onBluetoothStartDiscovery();
    void onMessageReceived(String message);
    void onMessageReceived(Object message);
    void onMessageReceived(byte[] message);
    void onBluetoothDisable();
    void onBluetoothNotSupported();
}
