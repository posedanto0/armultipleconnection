package wireless.network.manager;

public enum ConnectionState {
    CLIENT_DISCOVERING, SERVER_DISCOVERING, CLIENT_MESSAGING, SERVER_MESSAGING, DISABLE
}
