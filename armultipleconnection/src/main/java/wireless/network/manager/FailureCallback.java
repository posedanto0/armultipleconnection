package wireless.network.manager;

public interface FailureCallback {
    void onError();
}
